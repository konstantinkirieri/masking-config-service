package com.vslab.model

import javax.persistence.*

@Entity
@javax.persistence.Table(name = "TABLES")
class Table(
    @Id
    @GeneratedValue
    var id: Int? = null,
    var name: String,
    var description: String? = null,
    var isTransfer: Boolean = false,
    var isTruncate: Boolean = false,
    var selectionCriteria: String? = null,
    @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL], orphanRemoval = true)
    val attributes: MutableList<Attribute> = mutableListOf(),
)
