package com.vslab.model

import javax.persistence.*

@Entity
@javax.persistence.Table(name = "STORES")
class Store constructor(
    @Id
    var name: String,
    var description: String? = null,
    @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL], orphanRemoval = true)
    val tables: MutableList<Table> = mutableListOf(),
)
