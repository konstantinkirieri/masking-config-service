package com.vslab.repository

import com.vslab.model.Store
import org.springframework.data.repository.CrudRepository

interface StoreRepository : CrudRepository<Store, Long> {

    fun findByName(name: String): Store?

}
