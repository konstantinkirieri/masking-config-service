package com.vslab.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
@javax.persistence.Table(name = "ATTRIBUTES")
class Attribute(
    @Id
    @GeneratedValue
    var id: Int? = null,
    var name: String,
    var description: String? = null,
    var typePersonalInformation: String
)