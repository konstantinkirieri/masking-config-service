package com.vslab.controller

import com.vslab.dto.StoreDto
import com.vslab.dto.fromEntity
import com.vslab.dto.toEntity
import com.vslab.model.Store
import com.vslab.repository.StoreRepository
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
class ConfigServiceController(private val storeRepository: StoreRepository) {

    @GetMapping("/models/{name}")
    fun getModelByName(@PathVariable name: String): StoreDto? = storeRepository.findByName(name)?.fromEntity()

    @GetMapping("/models")
    fun getModels(): List<StoreDto> = storeRepository.findAll().map(Store::fromEntity)

    @PutMapping("/model")
    fun putModel(@RequestBody store: StoreDto): String = storeRepository.save(store.toEntity()).name
}

