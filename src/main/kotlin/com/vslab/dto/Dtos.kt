package com.vslab.dto

import com.vslab.model.Attribute
import com.vslab.model.Store
import com.vslab.model.Table

data class StoreDto(val name: String, val description: String?, val tables: List<TableDto>? = emptyList())

fun Store.fromEntity() = StoreDto(name, description, tables.map(Table::fromEntity))
fun StoreDto.toEntity() = Store(name, description, (tables ?: mutableListOf()).map(TableDto::toEntity).toMutableList())

data class TableDto(
    val name: String,
    val description: String? = null,
    val isTransfer: Boolean = false,
    val isTruncate: Boolean = false,
    val selectionCriteria: String? = null,
    val attributes: List<AttributeDto>? = emptyList(),
)

fun Table.fromEntity() = TableDto(
    name,
    description,
    isTransfer,
    isTruncate,
    selectionCriteria,
    attributes.map(Attribute::fromEntity)
)

fun TableDto.toEntity() = Table(
    name = name,
    description = description,
    isTransfer = isTransfer,
    isTruncate = isTruncate,
    selectionCriteria = selectionCriteria,
    attributes = (attributes ?: mutableListOf()).map(AttributeDto::toEntity).toMutableList()
)

data class AttributeDto(val name: String, val description: String? = null, val typePersonalInformation: String)

fun Attribute.fromEntity() = AttributeDto(name, description, typePersonalInformation)
fun AttributeDto.toEntity() = Attribute(null, name, description, typePersonalInformation)
